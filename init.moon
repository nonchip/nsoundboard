_FILE_ARGS_ = {arg[0], ...}
loadkit = require 'loadkit'
lgi = require 'lgi'
lfs = require 'lfs'

GLib = lgi.GLib
Gtk = lgi.Gtk
Gst = lgi.Gst
Gio = lgi.Gio

class Player
  new: =>
    @gst = Gst.ElementFactory.make 'playbin', nil
    @gst.bus\add_watch GLib.PRIORITY_DEFAULT, @\watcher
    @volume 1
  __gc: => @destroy!
  destroy: =>
    @gst.state = 'NULL'
  watcher: (bus, msg)=>
    if msg.type.ERROR
      print 'ERROR:', msg\parse_error!.message
      @stop!
    elseif msg.type.EOS
      print 'EOS'
      @stop!
  stop: =>
    print 'STOP'
    @gst.state = 'NULL'
  volume: (volume=nil,localvol=false)=>
    if localvol
      print "LVOL", @mastervol * volume
      @gst.volume = @mastervol * volume
    else
      if volume
        print "MVOL", volume
        @mastervol=volume
      else
        print 'RVOL'
      @gst.volume = @mastervol
  play: (uri,volume=nil)=>
    @stop!
    @volume volume, ('number' == type volume)
    print 'PLAYING:', uri
    @gst.uri = uri
    @gst.state = 'PLAYING'

ends_with = (str, ending)->
  ending == "" or str\sub(-#ending) == ending

starts_with = (str, start)->
  start == "" or str\sub(1,#start) == start

do
  print 'using gst: %s'\format Gst._version
  GAPP = Gtk.Application.new 'de.nonchip.nsoundboard', {}
  player = Player!

  sounds = {}
  for file in lfs.dir 'sounds'
    if not starts_with file, '.'
      local label, volume
      if starts_with file, 'VOL'
        volume=tonumber file\sub(4)\match '([^-]*).*'
        label=file\match '[^-]*-(.*)%..*'
        if label
          label ..= ' ['..volume..']'
        else
          label = file
      else
        label=file\match '(.*)%..*'
        label = file unless label
        volume=1
      table.insert sounds, {
        uri: 'file://'..lfs.currentdir!..'/sounds/'..file
        :label
        :volume
      }
  cols = math.floor math.sqrt 1 + #sounds

  GAPP.on_activate = (using window)=>
    window = with Gtk.Window!
      .application = GAPP
      .title = 'NSoundBoard'
      .on_delete_event = (using player, GAPP)=>
        player\destroy!
        GAPP\quit!
      vbox = with Gtk.VBox!
        volume = with Gtk.Scale!
          .digits = 1
          \set_range 0,4
          \set_value 1
          \set_increments 0.1, 0.25
          .on_value_changed = (using player, volume)=>
            player\volume volume\get_value!
        \pack_start volume, false, false, 0
        grid = with Gtk.Grid!
          \set_row_homogeneous true
          \set_column_homogeneous true
          \attach (Gtk.Button {
            label: 'STOP'
            on_clicked: (using file, player)=>
              player\stop!
          }),0,0,1,1
          y=0
          x=1
          for file in *sounds
            \attach (Gtk.Button {
              label: file.label
              on_clicked: (using file, player)=>
                player\play file.uri, file.volume
            }),x,y,1,1
            x+=1
            if x==cols
              x=0
              y+=1
        \pack_end grid, true, true, 0
      \add vbox
    window\show_all!

  GAPP\run _FILE_ARGS_
