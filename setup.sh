#!/bin/zsh

export NSOUNDBOARD_PATH="$(dirname $(readlink -f $0))"
export NSOUNDBOARD_REAL_ROOT="$NSOUNDBOARD_PATH/.root"
export NSOUNDBOARD_SRC="$NSOUNDBOARD_PATH/.src"
export NSOUNDBOARD_ROOT="/tmp/.nsoundboard.$(uuidgen -t)-$(uuidgen -r)"

continue_stage=n
if [ -f "$NSOUNDBOARD_PATH/.continue_stage" ]
  then continue_stage=$(cat "$NSOUNDBOARD_PATH/.continue_stage")
fi

if [ -f "$NSOUNDBOARD_PATH/.continue_root" ]
  then NSOUNDBOARD_ROOT=$(cat "$NSOUNDBOARD_PATH/.continue_root")
fi

case $continue_stage in
  n)
    rm -f "$NSOUNDBOARD_PATH/.continue_stage"
    rm -rf "$NSOUNDBOARD_ROOT" "$NSOUNDBOARD_SRC" "$NSOUNDBOARD_REAL_ROOT"
    mkdir -p "$NSOUNDBOARD_REAL_ROOT" "$NSOUNDBOARD_SRC"
    ln -s "$NSOUNDBOARD_REAL_ROOT" "$NSOUNDBOARD_ROOT"
    echo "$NSOUNDBOARD_ROOT" > "$NSOUNDBOARD_PATH/.continue_root"
    ;&
  luajit) v=8271c643c21d1b2f344e339f559f2de6f3663191
    echo "luajit" > "$NSOUNDBOARD_PATH/.continue_stage"
    cd $NSOUNDBOARD_SRC
    git clone http://luajit.org/git/luajit-2.0.git luajit || exit
    cd luajit
    git checkout ${v}
    make amalg PREFIX=$NSOUNDBOARD_ROOT CPATH=$NSOUNDBOARD_ROOT/include LIBRARY_PATH=$NSOUNDBOARD_ROOT/lib CFLAGS='-DLUAJIT_ENABLE_LUA52COMPAT -DLUAJIT_ENABLE_GC64' && \
    make install PREFIX=$NSOUNDBOARD_ROOT || exit
    ln -sf $(find $NSOUNDBOARD_ROOT/bin/ -name "luajit-2.1*" | head -n 1) $NSOUNDBOARD_ROOT/bin/luajit
    ;&
  luarocks) v=d2718bf39dace0af009b9484fc6019b276906023
    echo "luarocks" > "$NSOUNDBOARD_PATH/.continue_stage"
    cd $NSOUNDBOARD_SRC
    git clone https://github.com/luarocks/luarocks.git || exit
    cd luarocks
    git checkout ${v}
    git pull
    ./configure --prefix=$NSOUNDBOARD_ROOT \
                --lua-version=5.1 \
                --lua-suffix=jit \
                --with-lua=$NSOUNDBOARD_ROOT \
                --with-lua-include=$NSOUNDBOARD_ROOT/include/luajit-2.1 \
                --with-lua-lib=$NSOUNDBOARD_ROOT/lib/lua/5.1 \
                --force-config && \
    make build && make install || exit
    ;&
  moonscript)
    echo "moonscript" > "$NSOUNDBOARD_PATH/.continue_stage"
    $NSOUNDBOARD_ROOT/bin/luarocks install moonscript || exit
    ;&
  lgi)
    echo "lgi" > "$NSOUNDBOARD_PATH/.continue_stage"
    $NSOUNDBOARD_ROOT/bin/luarocks install lgi || exit
    ;&
  loadkit)
    echo "loadkit" > "$NSOUNDBOARD_PATH/.continue_stage"
    $NSOUNDBOARD_ROOT/bin/luarocks install loadkit || exit
    ;&
  luafilesystem)
    echo "luafilesystem" > "$NSOUNDBOARD_PATH/.continue_stage"
    $NSOUNDBOARD_ROOT/bin/luarocks install luafilesystem || exit
    ;&
  wrappers)
    echo "wrappers" > "$NSOUNDBOARD_PATH/.continue_stage"
    # wrappers
    cat > $NSOUNDBOARD_PATH/.run <<END
#!/bin/zsh
export NSOUNDBOARD_PATH="\$(dirname "\$(readlink -f "\$0")")"
export NSOUNDBOARD_REAL_ROOT="\$NSOUNDBOARD_PATH/.root"
export NSOUNDBOARD_ROOT="$NSOUNDBOARD_ROOT"

[ -e "\$NSOUNDBOARD_ROOT" ] || ln -s "\$NSOUNDBOARD_PATH/.root" \$NSOUNDBOARD_ROOT

export PATH="\$NSOUNDBOARD_ROOT/bin:\$NSOUNDBOARD_ROOT/nginx/sbin:\$PATH"
export LD_LIBRARY_PATH="\$NSOUNDBOARD_ROOT/lib:\$LD_LIBRARY_PATH"

path_prefixes=(./custom_ \$NSOUNDBOARD_PATH/ \$NSOUNDBOARD_PATH/modules/ \$NSOUNDBOARD_ROOT/lualib/ \$NSOUNDBOARD_ROOT/share/luajit-2.1.0-beta3/ \$NSOUNDBOARD_ROOT/share/lua/5.1/)

LUA_PATH=""
LUA_CPATH=""
MOON_PATH=""

for prefix (\$path_prefixes)
  do LUA_PATH="\$LUA_PATH;\${prefix}?.lua;\${prefix}?/init.lua"
  LUA_CPATH="\$LUA_CPATH;\${prefix}?.so;"
  MOON_PATH="\$MOON_PATH;\${prefix}?.moon;\${prefix}?/init.moon"
done

export LUA_PATH
export LUA_CPATH
export MOON_PATH

fn=\$(basename \$0)
if [ "\$fn" = ".run" ]
  then exec "\$@"
else
  exec \$fn "\$@"
fi
END
    chmod a+rx $NSOUNDBOARD_PATH/.run
    ;&
  moonc_all)
    echo "moonc_all" > "$NSOUNDBOARD_PATH/.continue_stage"
    $NSOUNDBOARD_PATH/.run moonc $NSOUNDBOARD_PATH || exit
    ;&
esac

# cleanup
rm -rf "$NSOUNDBOARD_SRC"
rm -f "$NSOUNDBOARD_ROOT" "$NSOUNDBOARD_PATH/.continue_stage" "$NSOUNDBOARD_PATH/.continue_root"
