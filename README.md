# nsoundboard
A simple GStreamer based sound board.

I hacked this for my Pen&Paper RPG sessions.

## First time setup

    ./setup.sh

## usage

Put audio files into `sounds`.
If their file names start with `VOL{number}-`, their relative volume will be multiplied by that number (poor girl's mp3gain).

    ./nsoundboard

The slider at the top is the master volume.
